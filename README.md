# Polyjuice Draughts

A set of checkers to ensure that Matrix servers are set up correctly.
Polyjuice Draughts checks for things such as whether the Matrix server can be
discovered by clients and other servers, and whether CORS headers are sent.

Servers can be checked either from the command line (using `mix polyjuice.check
<servername>`), or in a Matrix room using the
[Igor](https://gitlab.com/uhoreg/igor) responder plugin.

## Installation

Clone this repository, and run `mix deps.get`, `mix.compile`.  You will need
rustc, unless you do not want to use the Igor responder and you remove `igor`
from the dependencies in `mix.exs`.

## Running

You can check a server by using `mix polyjuice.check <servername>`, or by using
the Igor responder in the `Polyjuice.Draughts.IgorResponder` and using the
`!servertest <servername>` command in a chat.  See the [Igor
README](https://gitlab.com/uhoreg/igor) for information about how to set up
Igor.
