defmodule PolyjuiceDraughts.MixProject do
  use Mix.Project

  def project do
    [
      app: :polyjuice_draughts,
      version: "0.0.1",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:hackney, "~> 1.12"},
      {:igor, "~> 0.3.0"},
      {:jason, "~> 1.2"},
      {:polyjuice_client, "~> 0.4.3"},
      {:polyjuice_server,
       git: "https://gitlab.com/uhoreg/polyjuice_server.git",
       ref: "8162a9a5f85e405364baad1525a4715af4cfc75d"}
    ]
  end
end
