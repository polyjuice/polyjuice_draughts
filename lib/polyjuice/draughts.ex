# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts do
  import Polyjuice.Draughts.LoggingRailway

  def check_client(server, opts) when is_binary(server) and is_list(opts) do
    output_module = opts[:output_module]
    output_args = opts[:output_args] || []
    intro = &apply(output_module, :intro, output_args ++ [&1])
    output = &apply(output_module, :output, output_args ++ [&1, &2])

    filter_ip_addresses =
      case opts[:ipv] do
        nil -> &Enum.map(&1, fn x -> elem(x, 1) end)
        4 -> &Polyjuice.Draughts.Util.filter_ip_addresses(&1, :ipv4)
        6 -> &Polyjuice.Draughts.Util.filter_ip_addresses(&1, :ipv6)
      end

    i1 = intro.("Discovering client-server location...")

    with {:ok, {hs_url, is_url}, log} <- Polyjuice.Draughts.CheckClient.discovery(server) do
      hs_host = URI.parse(hs_url).host
      hs_addresses = Polyjuice.Draughts.Util.get_ip_addresses(hs_host)
      hs_addresses_str = Enum.map(hs_addresses, &elem(&1, 1)) |> Enum.join(", ")

      is_addresses =
        case is_url do
          nil ->
            output.(
              {:ok,
               "Homeserver for #{server} discovered at #{hs_url} (IP addresses #{hs_addresses_str}).\nNo identity server discovered",
               log},
              i1
            )

            nil

          _ ->
            is_host = URI.parse(is_url).host
            is_addresses = Polyjuice.Draughts.Util.get_ip_addresses(is_host)
            is_addresses_str = Enum.map(is_addresses, &elem(&1, 1)) |> Enum.join(", ")

            output.(
              {:ok,
               "Homeserver for #{server} discovered at #{hs_url} (IP addresses #{hs_addresses_str}).\nIdentity server discovered at #{is_url} (IP addresses #{is_addresses_str})",
               log},
              i1
            )

            is_addresses
        end

      hs_addresses = filter_ip_addresses.(hs_addresses)

      i2 = intro.("Checking client-server spec version support...")

      Polyjuice.Draughts.CheckClient.versions(hs_url, hs_addresses) >>>
        (fn version ->
           {:ok, "Homeserver supports client-server spec versions #{Enum.join(version, ", ")}",
            nil}
         end).()
      |> output.(i2)

      i3 = intro.("Checking CORS support...")

      Polyjuice.Draughts.CheckClient.cors(hs_url, "_matrix/client/r0/login", hs_addresses) >>>
        (fn true ->
           {:ok, "Homeserver sends CORS headers", nil}
         end).()
      |> output.(i3)

      if is_url do
        is_addresses = filter_ip_addresses.(is_addresses)

        i4 = intro.("Checking identity server...")

        Polyjuice.Draughts.CheckClient.identity_server(hs_url, hs_addresses) >>>
          (fn _ ->
             {:ok, "Identity server gave a valid response", nil}
           end).()
        |> output.(i4)

        i5 = intro.("Checking CORS support for identity server...")

        Polyjuice.Draughts.CheckClient.cors(
          is_url,
          "_matrix/identity/v2/account/register",
          is_addresses
        ) >>>
          (fn true ->
             {:ok, "Identity server sends CORS headers", nil}
           end).()
        |> output.(i5)
      end
    else
      {:error, _} = err -> output.(err, i1)
    end
  end

  def check_federation(server, opts \\ []) when is_binary(server) and is_list(opts) do
    output_module = opts[:output_module]
    output_args = opts[:output_args] || []
    intro = &apply(output_module, :intro, output_args ++ [&1])
    output = &apply(output_module, :output, output_args ++ [&1, &2])

    filter_ip_addresses =
      case opts[:ipv] do
        nil -> &Enum.map(&1, fn x -> elem(x, 1) end)
        4 -> &Polyjuice.Draughts.Util.filter_ip_addresses(&1, :ipv4)
        6 -> &Polyjuice.Draughts.Util.filter_ip_addresses(&1, :ipv6)
      end

    i6 = intro.("Discovering federation...")

    with {:ok, locations, log} <- Polyjuice.Draughts.CheckFederation.discovery(server) do
      locations =
        Enum.map(locations, fn
          {:ip_literal, %{locations: [{addr, port}]} = l} ->
            address_type = if String.contains?(addr, ":"), do: :ipv6, else: :ipv4
            {:ip_literal, %{l | locations: [{addr, port, [{address_type, addr}]}]}}

          {method, %{locations: locations} = l} ->
            locations =
              Enum.map(locations, fn {addr, port} ->
                {addr, port, Polyjuice.Draughts.Util.get_ip_addresses(addr)}
              end)

            {method, %{l | locations: locations}}
        end)

      locations_str =
        Enum.map(locations, fn {method, %{locations: locations}} ->
          locations_str =
            Enum.map(locations, fn {addr, port, ip_addresses} ->
              ip_addresses_str = Enum.map(ip_addresses, &elem(&1, 1)) |> Enum.join(", ")
              "#{addr}:#{port} (#{ip_addresses_str})"
            end)
            |> Enum.join(", ")

          case method do
            :ip_literal ->
              Enum.map(locations, fn {addr, port, _ip_addresses} -> "#{addr}:#{port}" end)
              |> Enum.join(", ")

            :hostname_with_port ->
              "#{locations_str}"

            :well_known ->
              "#{locations_str} - via .well-known"

            :srv ->
              "#{locations_str} - via SRV record"

            :hostname_8448 ->
              "#{locations_str} - no .well-known or SRV"
          end
        end)
        |> Enum.join(", ")

      output.({:ok, "Using homeserver federation at #{locations_str}", log}, i6)

      # if both well-known and srv are given, just check well-known
      # FIXME: also check srv, but with the old semantics
      locations =
        case locations do
          [well_known: l1, srv: _] -> [well_known: l1]
          _ -> locations
        end

      Enum.each(locations, fn {_method, %{locations: locations, host: host, identity: identity}} ->
        Enum.each(locations, fn {addr, port, ip_addresses} ->
          ip_addresses = filter_ip_addresses.(ip_addresses)

          i7 = intro.("Checking server version...")

          Polyjuice.Draughts.CheckFederation.version(
            server,
            addr,
            port,
            host,
            identity,
            ip_addresses
          ) >>>
            (fn
               %{"name" => name, "version" => version} ->
                 {:ok, "Homeserver is running #{name} version #{version}", nil}

               %{"name" => name} ->
                 {:ok, "Homeserver is running #{name} (unknown version)", nil}

               _ ->
                 {:ok, "Homeserver is running an unknown server", nil}
             end).()
          |> output.(i7)

          i8 = intro.("Checking server key...")

          Polyjuice.Draughts.CheckFederation.key(server, addr, port, host, identity, ip_addresses) >>>
            (fn %{"verify_keys" => keys} ->
               keys_str =
                 Enum.map(keys, fn {name, %{"key" => key}} ->
                   "#{name}: #{key}"
                 end)
                 |> Enum.join(", ")

               {:ok, "Server key(s): #{keys_str}", nil}
             end).()
          |> output.(i8)
        end)
      end)
    else
      {:error, _} = err -> output.(err, i6)
    end
  end
end
