# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.Client do
  @type t :: %__MODULE__{
          host: String.t(),
          base_url: URI.t()
        }
  @enforce_keys [:host, :base_url]
  defstruct [
    :host,
    :base_url
  ]

  defimpl Polyjuice.Client.API do
    def call(%{host: host, base_url: base_url}, endpoint) do
      %Polyjuice.Client.Endpoint.HttpSpec{
        method: method,
        headers: headers,
        path: path,
        query: query,
        body: body,
        auth_required: auth_required,
        stream_response: stream_response
      } = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

      query = query || []

      headers = [{"Host", host} | headers]

      url =
        %{
          URI.merge(base_url, path)
          | query: URI.encode_query(query)
        }
        |> to_string()

      if auth_required do
        # we shouldn't be making any calls to endpoints that require auth, but...
        {:error, :auth_required}
      else
        case :hackney.request(
               method,
               url,
               headers,
               body,
               ssl_options: :hackney_connection.ssl_opts(String.to_charlist(host), [])
             ) do
          {:ok, status_code, resp_headers, client_ref} ->
            Polyjuice.Client.Endpoint.Proto.transform_http_result(
              endpoint,
              status_code,
              resp_headers,
              if stream_response do
                Polyjuice.Client.hackney_response_stream(client_ref)
              else
                {:ok, body} = :hackney.body(client_ref)
                body
              end
            )

          err ->
            # anything else is an error -- return as-is
            err
        end
      end
    end

    def room_queue(_client_api, _room_id, func) do
      # this client doesn't have a queue.  Just run the function.
      func.()
    end

    def transaction_id(_) do
      "#{Node.self()}_#{:erlang.system_time(:millisecond)}_#{:erlang.unique_integer()}"
    end

    def get_user_and_device(_) do
      {nil, nil}
    end

    def stop(_, _, _) do
      # don't need to do anything to stop it
      :ok
    end
  end

  @spec new(base_url :: String.t(), ip_address :: String.t()) :: t()
  def new(base_url, ip_address) when is_binary(base_url) and is_binary(ip_address) do
    parsed_url = URI.parse(base_url)
    host = parsed_url.host

    %Polyjuice.Draughts.Client{
      host: host,
      base_url: %{parsed_url | host: ip_address, authority: ip_address}
    }
  end
end
