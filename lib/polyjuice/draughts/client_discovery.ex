# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.ClientDiscovery do
  import Polyjuice.Draughts.LoggingRailway

  def discover_server(server_name) when is_binary(server_name) do
    get_for_server(server_name) >>>
      combine(get_homeserver(), get_identity_server())
    |> done()
  end

  def get_for_server(server_name) when is_binary(server_name) do
    url =
      %URI{
        scheme: "https",
        host: server_name,
        authority: server_name,
        path: "/.well-known/matrix/client",
        port: 443
      }
      |> to_string()

    # FIXME: manually handle redirects
    case :hackney.request(:get, url, [{"Origin", "https://dummy.uhoreg.ca"}], "",
           follow_redirect: true
         ) do
      {:ok, 404, _, _} ->
        {:error,
         "No file found at #{url}.  Clients will not be able to discover your server location.  Skipping client tests."}

      {:ok, code, _headers, _client_ref} when code != 200 ->
        {:error, "Unexpected HTTP code (#{code}) when fetching #{url}"}

      {:ok, 200, headers, client_ref} ->
        add_log(:hackney.body(client_ref)) >>>
          add_log(Jason.decode()) >>>
          tee(
            (fn _ ->
               allow_origin =
                 Polyjuice.Client.Endpoint.get_header(headers, "access-control-allow-origin")

               case allow_origin do
                 "*" -> {:ok, :ok, []}
                 "https://dummy.uhoreg.ca" -> {:ok, :ok, []}
                 nil -> {:ok, :ok, [{:info, "Missing CORS header access-control-allow-origin"}]}
                 _ -> {:ok, :ok, [{:info, "Incorrect CORS header access-control-allow-origin"}]}
               end
             end).()
          ) >>>
          tee(
            (fn _ ->
               case Polyjuice.Client.Endpoint.get_header(headers, "content-type") do
                 "application/json" ->
                   {:ok, nil, nil}

                 <<"application/json;", _::binary>> ->
                   {:ok, nil, nil}

                 content_type ->
                   {:ok, nil,
                    {:info,
                     "Well-known file at #{url} is served with Content-Type: #{content_type} instead of application/json"}}
               end
             end).()
          )
        |> change_error(
          "#{url} has invalid contents.  Clients will not be able to discover your server location.  Skipping client tests."
        )

      _ ->
        {:error,
         "Unable to fetch #{url}.  Clients will not be able to discover your server location.  Skipping client tests."}
    end
  end

  def get_homeserver(%{"m.homeserver" => %{"base_url" => base_url}}) do
    lift(URI.parse(base_url)) >>>
      fatal_tee(
        (fn %{host: host, scheme: scheme} ->
           if host == nil or (scheme != "http" and scheme != "https") do
             {:error, {:warning, "Invalid homeserver URL: #{base_url}"}}
           else
             {:ok, nil, nil}
           end
         end).()
      ) >>>
      lift(to_string)
  end

  def get_homeserver(_) do
    {:error, "Unable to get homeserver URL"}
  end

  def get_identity_server(%{"m.identity_server" => %{"base_url" => base_url}}) do
    lift(URI.parse(base_url)) >>>
      fatal_tee(
        (fn %{host: host, scheme: scheme} ->
           if host == nil or (scheme != "http" and scheme != "https") do
             {:halt, nil, {:warning, "Invalid identity server URL: #{base_url}"}}
           else
             {:ok, nil, nil}
           end
         end).()
      ) >>>
      lift(to_string)
  end

  def get_identity_server(_) do
    {:ok, nil, []}
  end
end
