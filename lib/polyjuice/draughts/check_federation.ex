# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.CheckFederation do
  require Logger

  def discovery(name) when is_binary(name) do
    Polyjuice.Draughts.FederationDiscovery.discover_server(name)
  end

  @doc ~S"""
  Checks that the server responds sensibly to the federation `/versions` endpoint.
  """
  def version(server_name, address, port, host, identity, ip_addresses)
      when is_binary(server_name) and is_binary(address) and is_integer(port) and is_binary(host) and
             is_binary(identity) and is_list(ip_addresses) do
    results =
      Enum.map(ip_addresses, fn addr ->
        response =
          Polyjuice.Draughts.FederationClient.new(host, identity, addr, port)
          |> Polyjuice.Server.Federation.Client.get_server_version(server_name)

        case response do
          {:error, {:tls_alert, {_, reason}}} ->
            {:error,
             "TLS error when trying to connect to #{address}:#{port} at IP address #{addr}: #{to_string(reason)}"}

          {:error, err} ->
            Logger.debug("Error connecting to #{addr}/#{address}:#{port}: #{inspect(err)}")
            # FIXME: more specific error messages
            {:error, "Unable to connect to #{server_name}:#{port} using IP address #{addr}"}

          {:error, _, _} ->
            {:error, "Server at IP address #{addr}:#{port} gave an invalid response"}

          {:ok, %{"server" => s}} ->
            {:ok, s}

          {:ok, _} ->
            {:error, "Server at IP address #{addr}:#{port} gave an invalid response"}
        end
      end)

    Polyjuice.Draughts.Util.collate_results(results)
  end

  @doc ~S"""
  Checks that the server has a valid key.
  """
  def key(server_name, address, port, host, identity, ip_addresses)
      when is_binary(server_name) and is_binary(address) and is_integer(port) and is_binary(host) and
             is_binary(identity) and is_list(ip_addresses) do
    now = :erlang.system_time(:millisecond)

    results =
      Enum.map(ip_addresses, fn addr ->
        response =
          Polyjuice.Draughts.FederationClient.new(host, identity, addr, port)
          |> Polyjuice.Server.Federation.Client.get_server_keys(server_name)

        case response do
          {:error, {:tls_alert, {_, reason}}} ->
            {:error,
             "TLS error when trying to connect to #{address}:#{port} at IP address #{addr}: #{to_string(reason)}"}

          {:error, :wrong_host} ->
            {:error,
             "Server key on #{address}:#{port} at IP address #{addr} belongs to the wrong host"}

          {:error, {:wrong_host, server_name}} ->
            {:error,
             "Server key on #{address}:#{port} at IP address #{addr} belongs to the wrong host (#{server_name})"}

          {:error, :bad_signature} ->
            {:error, "Server key on #{address}:#{port} at IP address #{addr} has a bad signature"}

          {:error, :missing_fields} ->
            {:error,
             "Server key on #{address}:#{port} at IP address #{addr} has the wrong format"}

          {:error, err} ->
            Logger.debug("Error connecting to #{addr}/#{address}:#{port}: #{inspect(err)}")
            # FIXME: more specific error messages
            {:error, "Unable to connect to #{server_name}:#{port} using IP address #{addr}"}

          {:error, _, _} ->
            {:error, "Server at IP address #{addr}:#{port} gave an invalid response"}

          {:ok, v} ->
            if Map.get(v, "valid_until_ts", 0) < now do
              {:error, "Server at IP address #{addr}:#{port} has an expired server key"}
            else
              response
            end
        end
      end)

    Polyjuice.Draughts.Util.collate_results(results, &Map.delete(&1, "valid_until_ts"))
  end
end
