# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.FederationClient do
  defstruct [
    :target_ip_address,
    :target_port,
    :host,
    :identity
  ]

  defimpl Polyjuice.Server.Federation.Client.API do
    def call(client, destination, endpoint) do
      httpspec = Polyjuice.Server.Federation.Endpoint.Proto.http_spec(endpoint, destination)

      path =
        if(String.starts_with?(httpspec.path, "/"), do: httpspec.path, else: "/" <> httpspec.path)

      headers = [{"Host", client.host} | httpspec.headers]

      url =
        %URI{
          scheme: "https",
          host: client.target_ip_address,
          port: client.target_port,
          path: path,
          query: if(httpspec.query, do: URI.encode_query(httpspec.query), else: nil)
        }
        |> to_string()

      body =
        if httpspec.body == nil do
          ""
        else
          {:ok, json} = Polyjuice.Util.JSON.canonical_json(httpspec.body)
          json
        end

      case :hackney.request(
             httpspec.method,
             url,
             headers,
             body,
             ssl_options: :hackney_connection.ssl_opts(to_charlist(client.identity), [])
           ) do
        {:ok, status_code, resp_headers, client_ref} ->
          body =
            if httpspec.stream_response do
              Polyjuice.Server.Federation.Client.hackney_response_stream(client_ref)
            else
              {:ok, body} = :hackney.body(client_ref)
              body
            end

          # FIXME: handle any status codes specially?
          Polyjuice.Server.Federation.Endpoint.Proto.transform_http_result(
            endpoint,
            status_code,
            resp_headers,
            body,
            destination
          )

        err ->
          # anything else is an error -- return as-is
          err
      end
    end
  end

  def new(host, identity, ip_address, port)
      when is_binary(host) and is_binary(identity) and is_binary(ip_address) and is_integer(port) do
    %Polyjuice.Draughts.FederationClient{
      target_ip_address: ip_address,
      target_port: port,
      host: host,
      identity: identity
    }
  end
end
