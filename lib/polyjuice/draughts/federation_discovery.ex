# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.FederationDiscovery do
  import Polyjuice.Draughts.LoggingRailway

  def discover_server(server_name) when is_binary(server_name) do
    {:ok, [], nil} >>>
      as_ip_literal(server_name) >>>
      as_hostname_with_port(server_name) >>>
      via_wellknown(server_name) >>>
      via_srv(server_name) >>>
      as_hostname_8448(server_name)
    |> done()
  end

  defp as_ip_literal([], server_name) do
    case Regex.run(
           ~r/^(\d+\.\d+\.\d+\.\d+|\[[0-9a-fA-F:]+(?::\d+\.\d+\.\d+\.\d+)?\])(?::(\d+))?$/,
           server_name
         ) do
      [_, addr] ->
        {:halt, [ip_literal: %{locations: [{addr, 8448}], host: addr, identity: addr}], nil}

      [_, addr, port] ->
        {:halt,
         [
           ip_literal: %{
             locations: [{addr, String.to_integer(port)}],
             host: server_name,
             identity: addr
           }
         ], nil}

      _ ->
        {:ok, [], nil}
    end
  end

  defp as_ip_literal(prev, _server_name) do
    {:ok, prev, nil}
  end

  defp as_hostname_with_port([], server_name) do
    # 2. If the hostname is not an IP literal, and the server name includes an
    # explicit port, resolve the IP address using AAAA or A records. Requests
    # are made to the resolved IP address and given port with a Host header of
    # the original server name (with port). The target server must present a
    # valid certificate for the hostname.
    case Regex.run(
           ~r/^([0-9a-zA-Z][-0-9a-zA-Z]*(?:\.[0-9a-zA-Z][-0-9a-zA-Z]*)*):(\d+)$/,
           server_name
         ) do
      [_, hostname, port] ->
        {:halt,
         [
           hostname_with_port: %{
             locations: [{hostname, String.to_integer(port)}],
             host: server_name,
             identity: hostname
           }
         ], nil}

      _ ->
        {:ok, [], nil}
    end
  end

  defp as_hostname_with_port(prev, _server_name) do
    {:ok, prev, nil}
  end

  defp via_wellknown(prev, server_name) do
    # 3. If the hostname is not an IP literal, a regular HTTPS request is made
    # to https://<hostname>/.well-known/matrix/server, expecting the schema
    # defined later in this section. 30x redirects should be followed, however
    # redirection loops should be avoided. Responses (successful or otherwise)
    # to the /.well-known endpoint should be cached by the requesting
    # server. Servers should respect the cache control headers present on the
    # response, or use a sensible default when headers are not present. The
    # recommended sensible default is 24 hours. Servers should additionally
    # impose a maximum cache time for responses: 48 hours is
    # recommended. Errors are recommended to be cached for up to an hour, and
    # servers are encouraged to exponentially back off for repeated
    # failures. The schema of the /.well-known request is later in this
    # section. If the response is invalid (bad JSON, missing properties,
    # non-200 response, etc), skip to step 4. If the response is valid, the
    # m.server property is parsed as <delegated_hostname>[:<delegated_port>]
    # and processed as follows:
    # FIXME: sanitize server name
    url =
      %URI{
        host: server_name,
        scheme: "https",
        port: 443,
        path: "/.well-known/matrix/server"
      }
      |> to_string()

    case :hackney.request(:get, url, [], "", follow_redirect: true) do
      {:ok, 200, _headers, client_ref} ->
        :hackney.body(client_ref) |> add_log()

      {:ok, 404, _, _} ->
        {:halt, prev, nil}

      {:ok, status, _, _} ->
        {:halt, prev,
         {:warning, "Unexpected HTTP code(#{status}) when fetching #{url}.  Ignoring well-known."}}

      _ ->
        {:halt, prev, nil}
    end >>>
      (&(add_log(&1, Jason.decode())
         |> (fn
               {:ok, _, _} = res ->
                 res

               {:error, _} ->
                 {:halt, prev,
                  {:warning, ".well-known file #{url} is not JSON.  Ignoring well-known."}}
             end).())).() >>>
      (&(case &1 do
           %{"m.server" => delegated_hostname} ->
             {:ok, [], []} >>>
               as_ip_literal(delegated_hostname) >>>
               as_hostname_with_port(delegated_hostname) >>>
               via_srv(delegated_hostname) >>>
               as_hostname_8448(delegated_hostname)
             |> done() >>>
               (fn [{method, location}] ->
                  {:ok, prev ++ [{:well_known, Map.put(location, :method, method)}], nil}
                end).()

           _ ->
             {:halt, prev,
              {:warning,
               ".well-known file at #{url} does not delegate to a server.  Ignoring well-known."}}
         end)).()
    |> done()
  end

  defp via_srv(prev, server_name) do
    # 4. If the /.well-known request resulted in an error response, a server is
    # found by resolving an SRV record for _matrix._tcp.<hostname>. This may
    # result in a hostname (to be resolved using AAAA or A records) and
    # port. Requests are made to the resolved IP address and port, using 8448
    # as a default port, with a Host header of <hostname>. The target server
    # must present a valid certificate for <hostname>.

    srv = :inet_res.lookup('_matrix._tcp.' ++ to_charlist(server_name), :in, :srv)

    case srv do
      [] ->
        {:ok, prev, nil}

      _ ->
        locations =
          srv
          |> Enum.sort(fn {prio1, wt1, _, _}, {prio2, wt2, _, _} ->
            # FIXME: use weight correctly (e.g. within the same priority,
            # randomly order the results so that the results with higher weight
            # are more likely to appear earlier)
            prio1 < prio2 || (prio1 == prio2 && wt1 >= wt2)
          end)
          |> Enum.map(fn {_, _, port, target} -> {to_string(target), port} end)

        # FIXME: warn if any location is a CNAME
        cnames =
          Enum.map(locations, &elem(&1, 0))
          |> Enum.filter(&(:inet_res.lookup(to_charlist(&1), :in, :cname) != []))

        log =
          if cnames == [] do
            nil
          else
            {:warning,
             "SRV record points to #{Enum.join(cnames, ", ")}, which is/are CNAME records.  SRV records cannot point CNAME records"}
          end

        {:ok, prev ++ [{:srv, %{locations: locations, host: server_name, identity: server_name}}],
         log}
    end
  end

  defp as_hostname_8448([], server_name) do
    {:halt,
     [
       hostname_8448: %{
         locations: [{server_name, 8448}],
         host: server_name,
         identity: server_name
       }
     ], nil}
  end

  defp as_hostname_8448(prev, _server_name) do
    {:ok, prev, nil}
  end
end
