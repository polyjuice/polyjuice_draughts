# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.MatrixOutput do
  import Polyjuice.Draughts.LoggingRailway

  defp send(client, room, msg) do
    case Polyjuice.Client.Room.send_message(client, room, msg) do
      {:ok, _} = ret ->
        ret

      {:error, 429, %{"errcode" => "M_LIMIT_EXCEEDED", "retry_after_ms" => retry}} ->
        Process.sleep(retry)
        send(client, room, msg)

      ret ->
        ret
    end
  end

  def intro(client, room, text) do
    msg = Polyjuice.Client.MsgBuilder.to_message(text, "m.notice")
    {:ok, msgid} = send(client, room, msg)
    msgid
  end

  def output(client, room, {:ok, text, log}, msgid) do
    text =
      [
        "✅ #{text}"
        | init_log(log)
          |> Enum.map(fn {type, text} ->
            case type do
              :warning -> " ⚠️ "
              :info -> " ℹ️ "
              :error -> " 🚨 "
            end <> text
          end)
      ]
      |> Enum.join("\n")

    msg = Polyjuice.Client.Message.edit(text, "m.notice", text, %{"event_id" => msgid})
    send(client, room, msg)
  end

  def output(client, room, {:error, texts}, msgid) when is_list(texts) do
    text =
      Enum.map(texts, fn text ->
        "🚨 #{text}"
      end)
      |> Enum.join("\n")

    msg = Polyjuice.Client.Message.edit(text, "m.notice", text, %{"event_id" => msgid})
    send(client, room, msg)
  end

  def output(client, room, {:error, text}, msgid) do
    text = "🚨 #{text}"
    msg = Polyjuice.Client.Message.edit(text, "m.notice", text, %{"event_id" => msgid})
    send(client, room, msg)
  end
end
