# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.Util do
  def collate_results(results, transform \\ & &1) do
    if Enum.all?(results, &match?({:error, _}, &1)) do
      {:error, Enum.map(results, &elem(&1, 1))}
    else
      log =
        Enum.map(results, fn
          {:error, _} = e -> e
          {:ok, _} -> nil
        end)
        |> Enum.filter(& &1)

      first_result =
        Enum.find_value(results, fn
          {:ok, val} -> val
          _ -> false
        end)

      log =
        if Enum.reduce(results, fn
             {:error, _}, y -> y
             x, {:error, _} -> x
             {:ok, x} = full, {:ok, y} -> if transform.(x) == transform.(y), do: full, else: false
             _, false -> false
           end) do
          # all the IP addresses gave the same result
          log
        else
          [{:warning, "The server did not return the same results on every IP address"} | log]
        end

      {:ok, first_result, log}
    end
  end

  def get_ip_addresses(hostname) when is_binary(hostname) do
    ipv4_addresses =
      :inet_res.lookup(to_charlist(hostname), :in, :a)
      |> Enum.map(fn {a, b, c, d} -> {:ipv4, "#{a}.#{b}.#{c}.#{d}"} end)

    ipv6_addresses =
      :inet_res.lookup(to_charlist(hostname), :in, :aaaa)
      |> Enum.map(fn addr ->
        {:ipv6,
         addr
         |> Tuple.to_list()
         |> Enum.map(&(Integer.to_string(&1, 16) |> String.downcase()))
         |> Enum.join(":")}
      end)

    Enum.concat(ipv4_addresses, ipv6_addresses)
  end

  def filter_ip_addresses(addresses, type) when is_list(addresses) and is_atom(type) do
    addresses
    |> Enum.filter(fn {t, _} -> t == type end)
    |> Enum.map(&elem(&1, 1))
  end
end
