# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.ConsoleOutput do
  import Polyjuice.Draughts.LoggingRailway

  def intro(text) do
    IO.write(text)
    IO.write("\r")
  end

  def output({:ok, text, log}, _) do
    IO.write("✅ ")
    IO.puts(text)

    init_log(log)
    |> Enum.each(fn {type, text} ->
      case type do
        :warning -> " ⚠️ "
        :info -> " ℹ️ "
        :error -> " 🚨 "
      end
      |> IO.write()

      IO.puts(text)
    end)
  end

  def output({:error, texts}, _) when is_list(texts) do
    Enum.each(texts, fn text ->
      IO.write("🚨 ")
      IO.puts(text)
    end)
  end

  def output({:error, text}, _) do
    IO.write("🚨 ")
    IO.puts(text)
  end
end
