# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.LoggingRailway do
  @moduledoc false
  # based on https://zohaib.me/railway-programming-pattern-in-elixir/
  # but allows functions to add log lines, and allows skipping steps
  def init_log(nil), do: []
  def init_log(log) when is_list(log), do: log
  def init_log(log), do: [log]

  def combine_logs(log1, log2) do
    log1 = init_log(log1)

    case log2 do
      nil -> log1
      x when is_list(x) -> log1 ++ log2
      _ -> log1 ++ [log2]
    end
  end

  defmacro left >>> right do
    quote do
      case unquote(left) do
        {:ok, x, log1} ->
          case x |> unquote(right) do
            {:ok, y, log2} -> {:ok, y, combine_logs(log1, log2)}
            {:halt, y, log2} -> {:halt, y, combine_logs(log1, log2)}
            {:error, _} = err -> err
          end

        {:halt, _, _} = res ->
          res

        {:error, _} = err ->
          err
      end
    end
  end

  def done(val) do
    case val do
      {:ok, res, log} -> {:ok, res, init_log(log)}
      {:halt, res, log} -> {:ok, res, init_log(log)}
      {:error, _} = err -> err
    end
  end

  defmacro combine(val, clause1, clause2) do
    quote bind_quoted: [val: val], unquote: true do
      case val |> unquote(clause1) do
        {:ok, y1, log1} ->
          case val |> unquote(clause2) do
            {:ok, y2, log2} -> {:ok, {y1, y2}, combine_logs(log1, log2)}
            {:error, _} = err -> err
          end

        {:error, _} = err ->
          err
      end
    end
  end

  defmacro lift(val) do
    quote do
      {:ok, unquote(val), nil}
    end
  end

  defmacro lift(val, f) do
    quote do
      {:ok, unquote(val) |> unquote(f), nil}
    end
  end

  defmacro add_log(val) do
    quote do
      case unquote(val) do
        {:ok, res} -> {:ok, res, []}
        {:error, _} = err -> err
      end
    end
  end

  defmacro add_log(val, f) do
    quote do
      case unquote(val) |> unquote(f) do
        {:ok, res} -> {:ok, res, []}
        {:error, _} = err -> err
      end
    end
  end

  def change_error(val, err) do
    case val do
      {:ok, _, _} = res -> res
      {:error, _} -> {:error, err}
    end
  end

  defmacro tee(val, f) do
    quote bind_quoted: [val: val], unquote: true do
      case val |> unquote(f) do
        {:ok, _, log} -> {:ok, val, log}
        _ -> {:ok, val, nil}
      end
    end
  end

  defmacro fatal_tee(val, f) do
    quote bind_quoted: [val: val], unquote: true do
      case val |> unquote(f) do
        {:ok, _, log} -> {:ok, val, log}
        res -> res
      end
    end
  end
end
