# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.CheckClient do
  require Logger

  @doc ~S"""
  Checks that clients can discover the location of a server.
  """
  def discovery(name) when is_binary(name) do
    Polyjuice.Draughts.ClientDiscovery.discover_server(name)
  end

  @doc ~S"""
  Checks that the server responds sensibly to the client-server `/versions` endpoint.
  """
  def versions(base_url, addresses)
      when is_binary(base_url) and is_list(addresses) do
    results =
      Enum.map(addresses, fn addr ->
        response =
          Polyjuice.Draughts.Client.new(base_url, addr)
          |> Polyjuice.Client.versions()

        case response do
          {:error, {:tls_alert, {_, reason}}} ->
            {:error,
             "TLS error when trying to connect to #{base_url} at IP address #{addr}: #{to_string(reason)}"}

          {:error, err} ->
            Logger.debug("Error connecting to #{base_url} at #{addr}: #{inspect(err)}")
            # FIXME: more specific error messages
            {:error, "Unable to connect to #{base_url} using IP address #{addr}"}

          {:error, _, _} ->
            {:error, "Server at IP address #{addr} gave an invalid response"}

          {:ok, %{"versions" => v}} ->
            {:ok, v}

          {:ok, _} ->
            {:error, "Server at IP address #{addr} gave an invalid response"}
        end
      end)

    Polyjuice.Draughts.Util.collate_results(results)
  end

  def identity_server(base_url, addresses)
      when is_binary(base_url) and is_list(addresses) do
    results =
      Enum.map(addresses, fn addr ->
        response =
          Polyjuice.Draughts.Client.new(base_url, addr)
          |> Polyjuice.Client.API.call(%Polyjuice.Draughts.EndpointIdentityGet{})

        case response do
          {:error, {:tls_alert, {_, reason}}} ->
            {:error,
             "TLS error when trying to connect to #{base_url} at IP address #{addr}: #{to_string(reason)}"}

          {:error, err} ->
            Logger.debug("Error connecting to #{base_url} at #{addr}: #{inspect(err)}")
            # FIXME: more specific error messages
            {:error, "Unable to connect to #{base_url} using IP address #{addr}"}

          {:error, _, _} ->
            {:error, "Server at IP address #{addr} gave an invalid response"}

          {:ok, %{}} ->
            {:ok, true}

          {:ok, _} ->
            {:error, "Server at IP address #{addr} gave an invalid response"}
        end
      end)

    Polyjuice.Draughts.Util.collate_results(results)
  end

  def cors(base_url, path, addresses)
      when is_binary(base_url) and is_list(addresses) do
    results =
      Enum.map(addresses, fn addr ->
        response =
          Polyjuice.Draughts.Client.new(base_url, addr)
          |> Polyjuice.Client.API.call(%Polyjuice.Draughts.EndpointOptions{path: path})

        case response do
          {:error, {:tls_alert, {_, reason}}} ->
            {:error,
             "TLS error when trying to connect to #{base_url} at IP address #{addr}: #{to_string(reason)}"}

          {:error, err} when is_binary(err) ->
            response

          {:error, err} ->
            Logger.debug("Error connecting to #{base_url} at #{addr}: #{inspect(err)}")
            # FIXME: more specific error messages
            {:error, "Unable to connect to #{base_url} using IP address #{addr}"}

          {:error, _, _} ->
            {:error, "Server at IP address #{addr} gave an invalid response"}

          :ok ->
            {:ok, true}
        end
      end)

    Polyjuice.Draughts.Util.collate_results(results)
  end
end
