# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Draughts.EndpointOptions do
  @moduledoc """
  Generic OPTIONS request.

  Pretends to want to POST to an endpoint.
  """

  @type t :: %__MODULE__{
          path: String.t()
        }

  defstruct [:path]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%{path: path}) do
      %Polyjuice.Client.Endpoint.HttpSpec{
        method: :options,
        path: path,
        headers: [
          {"Origin", "https://dummy.uhoreg.ca"},
          {"Access-Control-Request-Method", "POST"},
          {"Access-Control-Request-Headers", "Content-Type"}
        ],
        query: nil,
        body: "",
        auth_required: false,
        stream_response: false
      }
    end

    def transform_http_result(_req, status_code, headers, _body) do
      if status_code != 204 and status_code != 200 do
        # although 204 is the correct response to OPTIONS, 200 is OK
        {:error, "Unexpected status code: expected 204, got #{status_code}"}
      else
        allow_origin =
          Polyjuice.Client.Endpoint.get_header(headers, "access-control-allow-origin")

        allow_methods =
          Polyjuice.Client.Endpoint.get_header(headers, "access-control-allow-methods")

        allow_headers =
          Polyjuice.Client.Endpoint.get_header(headers, "access-control-allow-headers")

        if allow_origin && allow_methods && allow_headers do
          allow_origin_ok =
            allow_origin && (allow_origin == "https://dummy.uhoreg.ca" or allow_origin == "*")

          allow_methods_ok =
            allow_methods && String.split(allow_methods, ~r/, */) |> Enum.member?("POST")

          allow_headers_ok =
            allow_headers &&
              String.split(allow_headers, ~r/, */)
              |> Enum.map(&String.downcase/1)
              |> Enum.member?("content-type")

          if allow_origin_ok and allow_methods_ok and allow_headers_ok do
            :ok
          else
            bad_headers =
              [
                if(allow_origin_ok, do: [], else: ["Access-Control-Allow-Origin: #{allow_origin}"]),
                if(allow_methods_ok,
                  do: [],
                  else: ["Access-Control-Allow-Methods: #{allow_methods}"]
                ),
                if(allow_headers_ok,
                  do: [],
                  else: ["Access-Control-Allow-Headers: #{allow_origin}"]
                )
              ]
              |> Enum.concat()
              |> Enum.join(", ")

            {:error, "CORS headers do not allow requests: #{bad_headers}"}
          end
        else
          missing =
            [
              if(allow_origin, do: [], else: ["Access-Control-Allow-Origin"]),
              if(allow_methods, do: [], else: ["Access-Control-Allow-Methods"]),
              if(allow_headers, do: [], else: ["Access-Control-Allow-Headers"])
            ]
            |> Enum.concat()
            |> Enum.join(", ")

          {:error, "CORS headers missing: #{missing}"}
        end
      end
    end
  end
end
