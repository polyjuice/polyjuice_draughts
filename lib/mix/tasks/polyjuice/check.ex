# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Mix.Tasks.Polyjuice.Check do
  @moduledoc """
  Check a Matrix server.

      mix check [options] server_name

  ## Command line options

  * `--ipv=[4|6]` only test IPv4/6 addresses.

  """
  @shortdoc "Check a Matrix server."
  use Mix.Task

  @impl Mix.Task
  def run(args) do
    Mix.Task.run("app.start", [])

    with {opts, [server]} <-
           OptionParser.parse!(args,
             strict: [
               ipv: :integer
             ]
           ) do
      IO.puts("Checking #{server}:")
      opts = [{:output_module, Polyjuice.Draughts.ConsoleOutput} | opts]
      Polyjuice.Draughts.check_client(server, opts)
      Polyjuice.Draughts.check_federation(server, opts)
    else
      _ ->
        Mix.Task.run("help", ["polyjuice.check"])
    end
  end
end
